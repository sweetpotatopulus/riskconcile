# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 00:08:33 2020.

@author: raul
"""

import datetime
from pathlib import Path
from os import listdir
import sys
import pandas as pd
import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow.keras import layers
from tensorflow.keras.layers import Dense
import sklearn.model_selection
import numpy as np


sys.path.insert(0, 'C:\\Users\\raul\\Documents\\MAI\\Riskconcile\\riskconcile')
sys.path.insert(
    0, '/vsc-hard-mounts/leuven-data/335/vsc33596/workspace/riskconcile/')
tf.keras.backend.set_floatx('float32')
np.random.seed(1)


class InputLayer(layers.Layer):
    """Input layer.

    Receives as input the values for the option plus the parameters for the
    model. The later corresponding neurons can be trained.
    """

    def __init__(self, num_params, num_envals, **kwargs):
        """Define training neurons."""
        super(InputLayer, self).__init__(**kwargs)
        # Create a trainable input layer
        self.paramets = tf.Variable(initial_value=tf.ones((1, num_params),
                                                          dtype='float32'),
                                    trainable=True, name='Heston_parameters',
                                    dtype='float32')
        # Neuron for the Strike, Spot, etc -> we never train this layer
        self.envars = tf.Variable(initial_value=tf.ones((1, num_envals),
                                                        dtype='float32'),
                                  trainable=False, name='envar',
                                  dtype='float32')
        # self.first_lay = tf.concat([self.paramets,self.envars],1)

    def call(self, inputs):
        """Call the layer."""
        first_lay = tf.concat([self.paramets, self.envars], 1)
        return tf.multiply(inputs, first_lay)


class Calnet(tf.keras.Model):
    """End-to-end model for training."""

    def __init__(self,
                 num_params,
                 num_envals,
                 name='calnet',
                 **kwargs):
        super(Calnet, self).__init__(name=name, **kwargs)
        self.num_params = num_params
        self.num_envals = num_envals
        self.in_layer = InputLayer(num_params, num_envals)
        self.d1 = Dense(400, activation=tf.nn.leaky_relu)
        self.d2 = Dense(400, activation=tf.nn.leaky_relu)
        self.d3 = Dense(400, activation=tf.nn.leaky_relu)
        self.d4 = Dense(400, activation=tf.nn.leaky_relu)
        self.d5 = Dense(400, activation=tf.nn.leaky_relu)
        self.regression = Dense(1, activation='linear')

    def call(self, inputs):
        x = self.in_layer(inputs)
        x = self.d1(x)
        x = self.d2(x)
        x = self.d3(x)
        x = self.d4(x)
        x = self.d5(x)
        return self.regression(x)


# Train Step
@tf.function
def train_step(data, target):
    """Calculate the gradients and update the withs in the forward pass."""
    with tf.GradientTape() as tape:
        predictions = model(data, training=True)
        loss = loss_object(target, predictions)
    gradients = tape.gradient(loss, model.trainable_variables[1:])
    optimizer.apply_gradients(zip(gradients, model.trainable_variables[1:]))

    train_loss(loss)
    train_mae(target, predictions)
    train_mape(target, predictions)


@tf.function
def test_step(data, target):
    """Calculate the metrics in the testing step."""
    predictions = model(data, training=False)
    t_loss = loss_object(target, predictions)

    test_loss(t_loss)
    test_mae(target, predictions)
    test_mape(target, predictions)


@tf.function
def test_step_bw(data, target):
    """Calculate the testing metrics on the backward pass."""
    predictions = model(data, training=False)
    t_loss = loss_object(target, predictions)

    bw_test_loss(t_loss)
    bw_test_mae(target, predictions)
    bw_test_mape(target, predictions)


@tf.function
def backward_step(data, target):
    """Compute the radients and update the weight in the backwards pass."""
    with tf.GradientTape() as tape:
        predictions = model(data, training=True)
        loss = loss_object(target, predictions)
    gradients = tape.gradient(loss, [model.trainable_variables[0]])
    bw_opimizer.apply_gradients(zip(gradients, [model.trainable_variables[0]]))

    bw_loss(loss)
    bw_mae(target, predictions)
    bw_mape(target, predictions)


if __name__ == "__main__":
    # Training metrics
    loss_object = tf.keras.losses.MeanSquaredError()
    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_mae = tf.keras.metrics.MeanAbsoluteError(name='train_mae')
    train_mape = tf.keras.metrics.MeanAbsolutePercentageError(
        name='train_mape')
    # Testing metrics
    test_loss = tf.keras.metrics.Mean(name='test_loss')
    test_mae = tf.keras.metrics.MeanAbsoluteError(name='test_mae')
    test_mape = tf.keras.metrics.MeanAbsolutePercentageError(name='test_mape')
    # backward training metrics
    bw_loss = tf.keras.metrics.Mean(name='bw_loss')
    bw_mae = tf.keras.metrics.MeanAbsoluteError(name='bw_mae')
    bw_mape = tf.keras.metrics.MeanAbsolutePercentageError(name='bw_mape')
    # Backward testing metrics
    bw_test_loss = tf.keras.metrics.Mean(name='test_loss')
    bw_test_mae = tf.keras.metrics.MeanAbsoluteError(name='test_mae')
    bw_test_mape = tf.keras.metrics.MeanAbsolutePercentageError(
        name='test_mape')
    # Read all the csv files on the current folder, all of them contain the
    # training data
    csv_files = [f for f in listdir('./') if f.endswith('.csv')]
    data_frames = map(pd.read_csv, csv_files)
    theoretical_data = pd.concat(data_frames)
    print('Training with:', theoretical_data.shape)
    with open('stats.txt', 'w') as f:
        f.write(str(theoretical_data.shape))

    bach_size = 1024

    X_theory = theoretical_data[['sig', 'th', 'ka',
                                 'rho', 'eta', 'moneyness', 'r', 'q', 'T']].values
    Y_theory = theoretical_data['implied_vol'].values

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
        X_theory, Y_theory, test_size=.2)
    X_val, X_test, y_val, y_test = sklearn.model_selection.train_test_split(
        X_test, y_test, test_size=.5)
    X_train, X_test, y_train, y_test, X_val, y_val = [tf.convert_to_tensor(
        x_set, dtype='float32') for x_set in [X_train, X_test, y_train, y_test, X_val, y_val]]

    train_set = tf.data.Dataset.from_tensor_slices(
        (X_train, y_train)).batch(bach_size)
    val_set = tf.data.Dataset.from_tensor_slices(
        (X_val, y_val)).batch(bach_size*5)
    test_set = tf.data.Dataset.from_tensor_slices(
        (X_test, y_test)).batch(bach_size*5)
    batch_number = len(list(train_set))

    global_step = tf.Variable(0, trainable=False)
    learning_rate = tf.compat.v1.train.exponential_decay(learning_rate=.01,
                                                         global_step=global_step,
                                                         decay_steps=500*batch_number,
                                                         decay_rate=0.2,
                                                         staircase=True)
    # Forward pass optimizer
    radam = tfa.optimizers.RectifiedAdam(lr=1e-3,
                                         total_steps=8000 *
                                         len(theoretical_data)//bach_size,
                                         warmup_proportion=0.1,
                                         min_lr=1e-5)

    optimizer = tfa.optimizers.Lookahead(
        radam, sync_period=6, slow_step_size=0.5)
    # Backward pass optimizer
    bw_opimizer = tf.keras.optimizers.Adam()

    model = Calnet(5, 4)
    exp_name = '_radamLAhead'
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    train_log_dir = './logs/gradient_tape/' + current_time + exp_name + '/train'
    test_log_dir = './logs/gradient_tape/' + current_time + exp_name + '/test'
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)
    model_checkpoint_dir = './logs/model_checkpoints/' + current_time + exp_name + '/'

    Path(model_checkpoint_dir).mkdir(parents=True, exist_ok=True)

    EPOCHS = 8000
    checkpoint = True
    # model.set_weights(Weighs)

    for epoch in range(EPOCHS):
        train_loss.reset_states()
        train_mae.reset_states()
        train_mape.reset_states()
        test_loss.reset_states()
        test_mae.reset_states()
        test_mape.reset_states()

        for data, target in train_set:
            train_step(data, target)

        for val_data, val_target in val_set:
            test_step(val_data, val_target)

        if epoch % 50 == 0:
            # Save model checkpoint
            if checkpoint:
                model.save_weights(model_checkpoint_dir+'epoch_%s.h5' % epoch)
            template = 'Epoch %s:\nLoss: %f, MAE: %.6f, MAPE: %.4f, \nVal Loss: %f, Val MAE: %.6f, Val MAPE: %.4f\
            \n_____________________________________'
            print(template % (epoch+1,
                              train_loss.result(),
                              train_mae.result(),
                              train_mape.result(),
                              test_loss.result(),
                              test_mae.result(),
                              test_mape.result()))

            with train_summary_writer.as_default():
                tf.summary.scalar('loss', train_loss.result(), step=epoch)
                tf.summary.scalar('mae', train_mae.result(), step=epoch)
                tf.summary.scalar('mape', train_mape.result(), step=epoch)
            with test_summary_writer.as_default():
                tf.summary.scalar('loss', test_loss.result(), step=epoch)
                tf.summary.scalar('mae', test_mae.result(), step=epoch)
                tf.summary.scalar('mape', test_mape.result(), step=epoch)

    model.save_weights(model_checkpoint_dir+'final')
