# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:06:39 2020.

@author: raul
"""

from tools.evaluation_pipeline import run_experiments, run_iterations_experiment
from tools.ann import Ann
import tensorflow as tf


def main():
    """Run the project on the trained ANN."""
    model = Ann()
    run_experiments(model.calibrate, '_ANN_gpu', 2, 64, False)


if __name__ == '__main__':
    main()
