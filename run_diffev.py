# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:06:39 2020.

@author: raul
"""

from tools.evaluation_pipeline import run_experiments, run_iterations_experiment
from tools.models import differential_ev


def main():
    """Run the project on differential evolution."""
    # run_iterations_experiment(differential_ev, '_diffev', test_iter=2)
    run_experiments(differential_ev, '_diffev', 1, 32, False)


if __name__ == '__main__':
    main()
