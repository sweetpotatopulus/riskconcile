# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:06:39 2020.

@author: raul
"""

from tools.evaluation_pipeline import run_experiments, run_iterations_experiment
from tools.models import bayesian_opt, least_square_trf, global_and_local


def main():
    """Run the project on differential evolution."""
    # run_iterations_experiment(bayesian_opt, '_bayes', test_iter=10,
    #                           batches_size=[20, 50, 90, 130, 200, 300, 450, 700, 1000])
    run_experiments(bayesian_opt, '_bayes', 10, 16)


if __name__ == '__main__':
    main()
