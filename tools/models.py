# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:09:22 2020.

@author: raul
"""

from functools import partial
from hyperopt import fmin, Trials, hp, STATUS_OK, tpe
from hyperopt.fmin import generate_trials_to_calculate
import numpy as np
from scipy.optimize import differential_evolution, least_squares
import pandas as pd
from .utils import fft_price, residual_heston_diff_ev, residual_heston
from .evaluation_pipeline import get_dataset

# option_vals = (K, r, T, S0, q, Y)


def bayesian_opt(option_vals, initial_params, max_evals=100):
    """Optimize Heston using bayes opt."""
    if type(option_vals) == pd.core.frame.DataFrame:
        option_vals, _, _, _ = get_dataset(
            from_df=(option_vals, option_vals.head()))
    sig, the, kap, rho, eta = initial_params
    trials = generate_trials_to_calculate([{'sig': sig, 'th': the, 'ka': kap,
                                            'rho': rho, 'eta': eta}])

    space_heston = {'sig': hp.uniform('sig', .2, .8),
                    'th': hp.uniform('th', .01, .8),
                    'ka': hp.uniform('ka', 0, 3),
                    'rho': hp.uniform('rho', -.9, 0),
                    'eta': hp.uniform('eta', 0.01, .5), }

    def objective_heston(params, K, r, T, S0, q, Y, callput, diag):
        '''Residual function for the Heston parameter optimization.'''
        Y_pred = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston',
                           callput=callput, diag=diag, **params).reshape(-1,)
        if sum(~np.isfinite(Y_pred.reshape(-1,))) > 0:
            Y_pred[~np.isfinite(Y_pred)] = 9e30
        err = sum((Y-Y_pred)**2)/len(Y)
        return {'loss': err, 'parameters': params, 'status': STATUS_OK,
                'preds': Y_pred}

    (K, r, T, S0, q, Y) = option_vals
    fmin_objective_heston = partial(objective_heston, K=K, r=r, T=T,
                                    S0=S0, q=q, Y=Y, callput='c',
                                    diag='true')
    # hyperopt optimization
    best = fmin(fmin_objective_heston, space_heston, algo=tpe.suggest,
                trials=trials, max_evals=max_evals)
    x0 = (best['sig'], best['th'], best['ka'], best['rho'], best['eta'])
    best_loss = min(trials.losses())
    return x0, best_loss, max_evals


def differential_ev(option_vals, initial_params=False):
    if type(option_vals) == pd.core.frame.DataFrame:
        option_vals, _, _, _ = get_dataset(
            from_df=(option_vals, option_vals.head()))
    (K, r, T, S0, q, Y) = option_vals
    res = differential_evolution(func=residual_heston_diff_ev,
                                 args=(K, r, T, S0, q, Y, 'c', True),
                                 disp=False, bounds=[[0.2, 0.8], [0.01, 0.8],
                                                    [0.0, 3.0], [-0.9, 0.0],
                                                    [0.01, 0.5]],
                                 updating='deferred',
                                 popsize=50, workers=-1)
    return res.x, res.fun, res.nfev


def least_square_trf(option_vals, initial_params=False):
    """Do leats squares."""
    if type(option_vals) == pd.core.frame.DataFrame:
        option_vals, _, _, _ = get_dataset(
            from_df=(option_vals, option_vals.head()))
    (K, r, T, S0, q, Y) = option_vals
    try:
        res = least_squares(fun=residual_heston, x0=initial_params,
                            args=(K, r, T, S0, q, Y, 'c', True),
                            bounds=[[.2, 0.01, 0, -.9, 0.01],
                                    [.8, .8, 3, 0, .5]],
                            method='trf')
    except:
        res = least_squares(fun=residual_heston, x0=[.5, .5, .5, -.5, .5],
                            args=(K, r, T, S0, q, Y, 'c', True),
                            bounds=[[.2, 0.01, 0, -.9, 0.01],
                                    [.8, .8, 3, 0, .5]],
                            method='trf')
    return res.x, np.mean(sum((res.fun)**2)), res.nfev


def global_and_local(option_vals, initial_params=False):
    if type(option_vals) == pd.core.frame.DataFrame:
        option_vals, _, _, _ = get_dataset(
            from_df=(option_vals, option_vals.head()))
    x0, _, _ = bayesian_opt(option_vals, initial_params, max_evals=50)
    return least_square_trf(option_vals, x0)
