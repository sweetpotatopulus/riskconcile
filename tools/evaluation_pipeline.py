# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:09:22 2020.

@author: raul
"""

import datetime
from time import time
import os
from functools import reduce
import seaborn as sns
import numpy as np
import pandas as pd
from .utils import put2call, evaluate_on_test, plot_params, drop_tails


sns.set(style="whitegrid")


def get_optionvals_from_df(data):
    """Get option values from a  dataframe."""
    variables = [data.strike, data.rate, data.time_maturity, data.dividend]
    K, r, T, q = list(map(lambda x: x.tolist(), variables))
    S0 = data.price.tolist()
    Y = data.call_price.tolist()
    option_vals = (K, r, T, S0, q, Y)
    return option_vals


def get_dataset(len_data=int(1e10), path='./data/first_batch/',
                from_df=False):
    """Get the option values for dataset #1."""
    if from_df:
        train_df, test_df = from_df
        train_df = train_df.sample(n=min(len_data, len(train_df)))
    else:
        train_df = pd.read_csv(path + 'train.csv').head(len_data)
        test_df = pd.read_csv(path + 'test.csv')
    train_df = put2call(train_df)
    test_df = put2call(test_df)
    train_optionval = get_optionvals_from_df(train_df)
    test_optionval = get_optionvals_from_df(test_df)
    return train_optionval, test_optionval, train_df, test_df


def evaluate_calibration(func, option_vals, test_iter=1,
                         initial_params=[.5, .5, .5, -.5, .5],
                         tdf=pd.DataFrame()):
    """Run evaluation."""
    eval_params = []
    results = []
    if type(func) != type(evaluate_calibration):
        option_vals = tdf
    for _ in range(test_iter):
        init_t = time()
        params, cal_error, nfevals = func(option_vals, initial_params)
        eval_params.append(params)
        results.append((cal_error, time() - init_t, nfevals))

    return eval_params, results


def format_plot_and_save(errors, results, eval_params, train_df, test_df,
                         save_path, save=True):
    """Compute the agregates and plots, and save them."""
    errors['time'] = []
    errors['cal_error'] = []
    errors['cal_func_evals'] = []
    for (cal_e, cal_t, cal_nfev) in results:
        errors['cal_error'].append(cal_e)
        errors['time'].append(cal_t)
        errors['cal_func_evals'].append(cal_nfev)

    errors_df = pd.DataFrame(errors).agg(['mean', 'std'])
    eval_params = np.array(eval_params)
    mean_params = np.mean(eval_params, axis=0)
    if save:
        os.makedirs(save_path)
        print(save_path)
        errors_df.to_csv(save_path+'results.csv')
        plot_params(mean_params, train_df, save_path+'trainplot')
        plot_params(mean_params, test_df, save_path + 'testplot')
    return errors_df, mean_params


def first_batch_experiment(func, save_path, test_iter=10,
                           size_data=int(1e5), save=True):
    """Run evaluation on the datasets."""
    train_optionval, test_optionval, train_df, test_df = get_dataset(
        size_data)

    eval_params, results = evaluate_calibration(func, train_optionval,
                                                test_iter, tdf=train_df)
    errors = evaluate_on_test(eval_params, test_optionval)

    e_df = format_plot_and_save(errors, results, eval_params, train_df,
                                test_df, save_path, save)
    return e_df


def second_batch_experiment(func, save_path, test_iter=10,
                            size_data=int(1e5), save=True):
    """Evaluate models in the second batch of data by underlying and date."""
    _, _, train_df, test_df = get_dataset(path='./data/second_batch/')
    # Iterate for each UNDERLYING
    initial_params = [.5, .5, .5, -.5, .5]
    for und, underlying_train in train_df.groupby('underlying'):
        underlying_test = test_df[test_df.underlying == und]
        underlying_train = drop_tails(underlying_train, limitstd=2)
        und = und.replace('/', '-')
        print(und)
        # Iterate for each DATE
        for date_op, date_train in \
                underlying_train.sort_values('date').groupby('date'):
            date_test = underlying_test[underlying_test.date == date_op]
            # Get option values
            train_optionval, test_optionval, _, _ = get_dataset(
                size_data, from_df=(date_train, date_test))
            print('und: ', date_test.underlying.unique(),
                  date_train.underlying.unique())
            print('date: ', date_test.date.unique(), date_train.date.unique())
            # Calibrate and test
            eval_params, results = evaluate_calibration(func, train_optionval,
                                                        test_iter,
                                                        initial_params,
                                                        tdf=date_train)



            # print(results)
            errors = evaluate_on_test(eval_params, test_optionval)

            to_write = '\n'.join([str(x) for x in errors['MSE']])

            with open(save_path + str(und), 'a') as f:
                f.write(to_write+'\n')


            # print(errors)
            e_df, initial_params = format_plot_and_save(errors, results,
                                                        eval_params,
                                                        date_train, date_test,
                                                        save_path + '_%s/_%s/'
                                                        % (und, date_op),
                                                        save=save)
    return e_df


def run_experiments(func, exp_name, test_iter=10, size_data=int(1e5), save=True):
    """Run both of the experiments for a given optimizer."""
    # Define location for the saving
    current_time = datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S")
    first_path = './results2/' + current_time + exp_name + '/firstB/'
    # second_path = './results2/' + current_time + exp_name + '/second'
    second_path = './results2/' + current_time + exp_name
    # Run both experiments
    # first_batch_experiment(func, first_path, test_iter, size_data)
    second_batch_experiment(func, second_path, test_iter, size_data, save)


def run_iterations_experiment(func, exp_name, test_iter=100,
                              batches_size=2**np.arange(3, 11, 1)):
    """Evaluate the appropriate number of data needed to converge."""
    current_time = datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S")
    first_path = './results_iter/' + current_time + exp_name + '/firstB'
    batches_size = 2**np.arange(3, 11, 1)
    for size_data in batches_size:
        first_batch_experiment(
            func, first_path+'%s/' % size_data, test_iter, size_data)

# train_sample = data.sample(frac = .75).index
# train_data = data.loc[train_sample]
# test_data = data.loc[set(data.index)-set(train_sample)]
# train_data.to_csv('../data/first_batch/train.csv')
# test_data.to_csv('../data/first_batch/test.csv')
