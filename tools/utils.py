# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:09:22 2020.

@author: raul
"""

import os
from random import shuffle
from collections import defaultdict
from functools import partial, reduce
from collections.abc import Iterable
from time import time
import gc
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy import stats
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.optimize import bisect
import warnings


warnings.filterwarnings('ignore')
i = complex(0, 1)


def drange(x, y, jump):
    '''Sample numbers-range generator.'''
    return np.arange(x, y, jump)


def vgfftcf(u, p, r, t, sig, q, nu, th):
    '''Compute characteristic function for the VG model.'''
    vg = (1-(i*u*th*nu)+(sig**2*nu*u**2/2))**(-1/nu)
    y = np.exp(i*u * (np.log(p) + (r - q + vg) * t)) * \
        np.exp(vg*u**2*t)

    return y


def bsfftcf2(u, p, r, t, sig, q):
    '''Compute characteristic function for the BS model.'''
    y = np.exp(i*u * (np.log(p) + (r - q - (1 / 2) * sig**2) * t)) * \
        np.exp(-(1/2)*sig**2*u**2*t)
    return y


def hestonfftcf(th, u, ka, S0, rho, r, t, sig, q, eta):
    '''
    Characteristic function for the Heston model
    '''

    # Initial calculations
    d = ((rho*th*u*i - ka)**2 - th**2*(-i*u - u**2))**(1/2)
    g = (ka - rho*th*u*i - d) / (ka - rho*th*u*i + d)

    # Final answer
    y = np.exp(i*u*(np.log(S0) + (r-q)*t)) \
        * np.exp(eta*ka*th**-2*((ka - rho*th*u*i - d)*t - 2*np.log((1 - g*np.exp(-d*t))/(1-g)))) \
        * np.exp(sig**2*th**-2*(ka - rho*th*i*u - d)*(1 - np.exp(-d*t))/(1 - g*np.exp(-d*t)))

    return y


def batesfftcf(th, u, ka, S0, rho, r, t, sig, q, eta, lmb, nu, jet):
    '''Characteristic function for the Bates model'''
    # Initial calculations
    d = ((rho*th*u*i - ka)**2 - th**2*(-i*u - u**2))**(1/2)
    g = (ka - rho*th*u*i - d) / (ka - rho*th*u*i + d)

    # Final answer
    y = np.exp(i*u*(np.log(S0) + (r-q)*t)) \
        * np.exp(eta*ka*th**-2*((ka - rho*th*u*i - d)*t - 2*np.log((1 - g*np.exp(-d*t))/(1-g)))) \
        * np.exp(sig**2*th**-2*(ka - rho*th*i*u - d)*(1 - np.exp(-d*t))/(1 - g*np.exp(-d*t))) \
        * np.exp((-1*lmb*nu*i*u*t)+(lmb*t*(((1+nu)**(i*u))*np.exp((jet**2)*(i*u/2)*(i*u-1))-1)))
    return y


def fft_price(S0, r, q, K, T, model_name, verbose=False, callput='c',
              diag=True, **kwargs):
    # Carr-Madan reference values
    t0 = time()
    N = 4096
    i = complex(0, 1)
    alpha = 1.5
    etacm = 0.25

    # Validate data types
    if hasattr(T, '__iter__'):
        T = np.array(T).reshape(-1, 1)
    else:
        T = np.array([T]).reshape(-1, 1)
    if hasattr(r, '__iter__'):
        r = np.array(r).reshape(-1, 1)
    else:
        r = np.array([r]).reshape(-1, 1)
    if hasattr(S0, '__iter__'):
        S0 = np.array(S0).reshape(-1, 1)
        assert len(T) == len(S0), "T and S0 must have the same length"
    if hasattr(K, '__iter__'):
        K = np.array(K)
    else:
        if diag:
            K = [K]
    if hasattr(q, '__iter__'):
        q = np.array(q).reshape(-1, 1)
        assert len(T) == len(q), "T and q must have the same length"
    assert len(T) == len(r), "T and r must have the same length"

    # General calculations
    lmbd = 2*np.pi/N/etacm
    b = lmbd*N/2
    kcalc = np.array(drange(-b, b-(lmbd/2), lmbd)).reshape(1, -1)
    KK = np.exp(kcalc)
    v = np.array(drange(0, (N)*etacm, etacm)).reshape(1, -1)
    sw = (3 + np.power(-1, np.array(range(1, N+1))))
    sw[0] = 1
    sw = (sw/3).reshape(1, -1)

    # Working with the denominator
    partialnum = np.exp(-r*T)
    partialden = alpha**2+alpha-v**2+i*(2*alpha+1)*v

    partialop = partialnum/partialden

    u = v-(alpha+1)*i

    # Model Selection
    if model_name == 'BS':
        op = partialop * bsfftcf2(u=u, p=S0, r=r, t=T, q=q, **kwargs)
    elif model_name == 'Heston':
        op = partialop * hestonfftcf(u=u, S0=S0, r=r, t=T, q=q, **kwargs)
    elif model_name == 'Bates':
        op = partialop * batesfftcf(u=u, S0=S0, r=r, t=T, q=q, **kwargs)
    elif model_name == 'VG':
        op = partialop * vgfftcf(u=u, p=S0, r=r, t=T, q=q, **kwargs)
    else:
        raise ValueError('Please insert a valid model: "BS",\
                         "Heston" or "Bates"')

    A = op * np.exp(i * v * b) * etacm * sw

    if verbose:
        t1 = time()
        print("time taken in algebraic operations: ", t1-t0)

    Z = np.array(list(map(np.fft.fft, A))).real

    if verbose:
        t2 = time()
        print("time taken in FFT: ", t2-t1)

    CallPricesBS = np.exp(-alpha * kcalc) / np.pi * Z

    if diag:
        CallPricesL = list(map(lambda x: interp1d(KK.reshape(-1), x[0],
                                                  kind='cubic')(x[1]),
                               zip(CallPricesBS, K)))

    else:
        CallPricesL = list(map(lambda x: interp1d(KK.reshape(-1), x,
                                                  kind='cubic', fill_value='extrapolate')(K),
                               CallPricesBS))
    if verbose:
        t3 = time()
        print("time taken in interpolation", t3-t2)

    price = np.array(CallPricesL)
    if sum(~np.isfinite(price.reshape(-1,))) > 0:
        price[~np.isfinite(price)] = 9e30
    if callput == 'p':
        if diag:
            price += K*(np.exp(-r*T)).reshape(-1,
                                              )-(S0*np.exp(-q*T)).reshape(-1,)
        else:
            price += K*(np.exp(-r*T))-(S0*np.exp(-q*T))

    return price


def implied_volatility(S0, r, q, K, T, optionType, optPrice, low=0, high=1):
    '''Function to estimate the implied volatility'''
    def func(x):
        # function returns a value of the residual given a volatility
        y = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='BS',
                      callput=optionType, diag=True, sig=x)-optPrice
        return y
    # use bisect to find the value for the volatility
    return bisect(func, low, high)


def plot_params(params, data, save_path=False):
    '''This function plots the curves of a given calibration with different maturities'''
    sig, th, ka, rho, eta = params
    S0 = data.price.mean()
    T = data.time_maturity.unique()
    shuffle(T)
    T = T[:5]
    r = [data.rate.mean()]*len(T)
    q = [0]*len(T)
    data = data[data.time_maturity.map(lambda x: x in T)]
    mink = data.strike.min()
    maxk = data.strike.max()
    K = list(range(int(np.floor(mink)), int(np.ceil(maxk))+1))

    Y_call = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston', callput='c', diag=False,
                       sig=sig, th=th, ka=ka, rho=rho, eta=eta)

    Y_put = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston', callput='p', diag=False,
                      sig=sig, th=th, ka=ka, rho=rho, eta=eta)

    fig = plt.figure(figsize=(8, 5), dpi=80, facecolor='w', edgecolor='k')
    n_times = len(T)

    df_plot = pd.DataFrame()
    df_plot['strike'] = K*n_times
    df_plot['eu_price'] = Y_call.reshape(-1,)
    df_plot['putp'] = Y_put.reshape(-1,)
    df_plot['moneyness'] = S0/df_plot['strike']
    inter_time = len(K)
    df_plot['Time Maturity'] = reduce(
        lambda x, y: x+y, [[x]*inter_time for x in T])

    # Plot call prices
    sns.lineplot(x="moneyness", y="eu_price", hue="Time Maturity", data=df_plot, legend='full',
                 palette=sns.color_palette("Blues_d", n_times))
    sns.scatterplot(x="moneyness", y="eu_price", hue="time_maturity", data=data[data.call_put == 'C'],
                    legend=False, palette=sns.color_palette("Blues_d", len(data[data.call_put == 'C'].time_maturity.unique())))

    # Plot put prices
    sns.lineplot(x="moneyness", y="putp", hue="Time Maturity", data=df_plot, legend=False,
                 palette=sns.color_palette("Greens_d", n_times))
    fig1 = sns.scatterplot(x="moneyness", y="eu_price", hue="time_maturity",
                           data=data[data.call_put == 'P'], legend=False,
                           palette=sns.color_palette("Greens_d", len(data[data.call_put == 'P'].time_maturity.unique())))

    # plt.title("%s: Theoretical vs Real values" % data.underlying.iloc[0])
    plt.xlabel('Spot/Strike Price')
    plt.ylabel('European option price')
    plt.legend([str(x)[:5] for x in T], title='Time Maturity')
    # plt.axvline(S0, color='#591818')
    # ax = plt.gca()
    # # ax.set_xticks([mink, maxk, S0])
    # # ax.set_xticklabels([mink, maxk, "s0"])
    plt.close(fig)
    if save_path:
        fig1.figure.savefig(save_path + '_%s' %
                            (data.underlying.iloc[0].replace('/', '-')))


def drop_tails(df,
               columns=['strike', 'eu_price', 'iv',
                        'time_maturity', 'rate', 'dividend'],
               limitstd=3):
    """Drop the tails on all coluns of a dataframe."""
    usesful_cols = df.std()[(df.std() != 0)]
    columns = [x for x in columns if x in usesful_cols]
    z = np.abs(stats.zscore(df[columns]))
    return df[(z < limitstd).all(axis=1)]


def robust_interpolation(x, interpolations):
    """Interpolate using a dictionary."""
    interp_obj = interpolations[x[0]]
    if interp_obj == 0:
        return 0
    else:
        return interp_obj(x[1])


def get_file_name(files, name):
    """Get the ful name of the file."""
    return [x for x in files if name in x][0]


def interp_dict(data, group, x_inter, y_inter, log_inter=False):
    """Generate the interpolation dictionaries."""
    interpolations = defaultdict(lambda: 0)
    fail = []
    for x in data.groupby(group)[[x_inter, y_inter]]:
        if len(x[1]) > 5:
            if log_inter:
                interpolations[x[0]] = interp1d(
                    x[1][x_inter], x[1][y_inter], kind='cubic',
                    fill_value="extrapolate")
#                 interpolations[x[0]] = get_loginterp((.0, .01, .001, 0), (x[1][x_inter].min()-1e-10),
#                                                      x[1][x_inter], x[1][y_inter])

            else:
                interpolations[x[0]] = interp1d(
                    x[1][x_inter], x[1][y_inter], kind='quadratic',
                    fill_value="extrapolate")
        else:
            fail.append(x[0])
    return interpolations


def interpolate_by_groups(df, col2interp, interp_df, group4interp, x_inter,
                          y_inter, log_inter=False):
    interpolations = interp_dict(
        interp_df, group4interp, x_inter, y_inter, log_inter)

    return df[[group4interp, col2interp]].apply(partial(
        robust_interpolation, interpolations=interpolations), axis=1)


def get_data_folder(folder, filter_index=False):
    """
    Get and process the data from a given folder.

    :folder: A folder that contains the dividend, interests, option and
    spot DataFrame
    :filter_index: If required a list of index can be provided to exclude
    a set of values
    """
    files = os.listdir(folder)
    trading_days = 365
    dividendsdf = pd.read_csv(
        folder + get_file_name(files, 'dividend'), index_col=0)
    interestdf = pd.read_csv(folder + get_file_name(files, 'interest_rate'))
    optionsdf = pd.read_csv(folder + get_file_name(files, 'option'))
    optionsdf = optionsdf[optionsdf.open_interest != 0]
    spotdf = pd.read_csv(folder + get_file_name(files, 'spot'))
    # adjusting coluns for dividends
    dividendsdf.reset_index(inplace=True)
    dividendsdf.rename(columns={'tenor_date': 'expiration',
                                'dividend_yield': 'dividend'
                                }, inplace=True)
    df = optionsdf[['date', 'strike', 'eu_price',
                    'underlying', 'iv', 'call_put', 'expiration']].copy()
    # Computing maturity
    df.loc[:, 'time_maturity'] = (pd.to_datetime(
        optionsdf.expiration) - pd.to_datetime(optionsdf.date)).astype(
        'timedelta64[D]')/trading_days
    print("Initial number of rows: ", len(optionsdf))
    # Computing dividend yield
    df = df.merge(dividendsdf, on=['date', 'underlying', 'expiration']).copy()
    print("Number of rows after merging", len(df))
    df.drop('expiration', axis=1, inplace=True)
    # Dropping nans
    df.dropna(inplace=True)
    print("Number of rows after dropping nans", len(df))
    if filter_index:
        df = df.loc[filter_index]

    df.loc[:, 'rate'] = interpolate_by_groups(df, 'time_maturity', interestdf,
                                              'date', 'tenor_float', 'rate')
    print("before interpolaion", len(df))
    df = df.merge(spotdf[['date', 'underlying', 'price']],
                  on=['date', 'underlying'], how='left').copy()
    print("after merging spot", len(df))
    df.dropna(inplace=True)
    print("Number of removed rows after dropping NA: ", len(df))

    # Removal of 0s in prices
    df = df[df.eu_price > 0]
    print("Number of removed rows after dropping 0s in EU prices: ", len(df))
    # Removal of too short maturities
    df = df[df.time_maturity >= 15/trading_days]
    print("Number of removed rows after dropping maturities below one month: ",
          len(df))
    df.loc[:, 'moneyness'] = df['price']/df['strike']
    df = df[df.moneyness < 3]
    print("Number of removed rows after cleaning moneyness: ", len(df))
    z = np.abs(stats.zscore(
        df[['strike', 'eu_price', 'iv', 'time_maturity', 'rate', 'dividend']]))
    df_cleaniqr = df[(z < 3).all(axis=1)]
    print("Final number number of rows afer the 'Z score' outlier detection: ",
          len(df_cleaniqr))
    df_cleaniqr.loc[:, 'day_values'] = 1
    agg_df = df_cleaniqr.groupby(['underlying', 'date']).count()[
        ['day_values']].copy()
    df_cleaniqr.drop('day_values', axis=1, inplace=True)
    agg_df.reset_index(inplace=True)
    df_cleaniqr = df_cleaniqr.merge(agg_df, on=['underlying', 'date']).copy()
    df_cleaniqr = df_cleaniqr[df_cleaniqr.day_values > 80]
    df_cleaniqr.drop('day_values', axis=1, inplace=True)
    print("Number of rows after discarding days with little data",
          len(df_cleaniqr))
    del dividendsdf, interestdf, optionsdf, spotdf, df, agg_df
    gc.collect()
    return df_cleaniqr


def put2call(df_cleaniqr):
    df_cleaniqr['call_price'] = 0
    df_cleaniqr.loc[df_cleaniqr.call_put == 'C',
                    'call_price'] = df_cleaniqr.loc[df_cleaniqr.call_put == 'C', 'eu_price']
    put_df = df_cleaniqr[df_cleaniqr.call_put == 'P']
    df_cleaniqr.loc[df_cleaniqr.call_put == 'P', 'call_price'] = put_df.eu_price - (put_df.strike*np.exp(-(
        put_df.rate*put_df.time_maturity))) + (put_df.price*np.exp(-(put_df.dividend*put_df.time_maturity)))

    assert (df_cleaniqr.call_price == 0).sum(
    ) == 0, "You have prices equal to 0, check the call to put conversion"
    # df_cleaniqr.drop('call_put',axis=1,inplace=True)
    return df_cleaniqr


def residual_heston_diff_ev(params, K, r, T, S0, q, Y, callput, diag):
    '''
    Residual function for the Heston parameter optimization.
    '''
    sig, th, ka, rho, eta = params
    try:
        Y_pred = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston',
                           sig=sig, th=th, ka=ka, rho=rho, eta=eta,
                           callput=callput, diag=True).reshape(-1,)
        mse_np = sum((Y-Y_pred)**2)/len(Y)
    except:
        mse_np = 1e10
    return mse_np


def residual_heston(params, K, r, T, S0, q, Y, callput, diag):
    '''Residual function for the Heston parameter optimization.'''
    sig, th, ka, rho, eta = params

    Y_pred = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston',
                       sig=sig, th=th, ka=ka, rho=rho, eta=eta,
                       callput=callput, diag=True).reshape(-1,)
    residual = Y - Y_pred
    return residual


def evaluate_on_test(parameters, option_vals):
    """Evaluate a set of parameters in an option."""
    if isinstance(parameters[0], Iterable):
        num_params = len(parameters)
    else:
        num_params = 1
        parameters = [parameters]
    (K, r, T, S0, q, Y) = option_vals
    errors = {'MSE': [],
              'MAPE': [],
              'MAE': []}
    for i in range(num_params):
        sig, th, ka, rho, eta = parameters[i]
        Y_pred = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Heston',
                           sig=sig, th=th, ka=ka, rho=rho, eta=eta,
                           callput='c', diag=True).reshape(-1,)
        # for x, y in zip(Y, pd.read_csv('./data/second_batch/test.csv').eu_price.values):
        #     print(x, y)
        print(sig, th, ka, rho, eta)
        mse = sum((Y-Y_pred)**2)/len(Y)
        mae = sum(np.abs(Y-Y_pred))/len(Y)
        mape = sum(np.abs((Y-Y_pred)/Y))/len(Y)
        errors['MSE'].append(mse)
        errors['MAE'].append(mae)
        errors['MAPE'].append(mape)
    return errors


def get_printable(S0, r, q, K, T, sig, th, ka, rho, eta):
    """Compute and validate the price and its BS volatility."""
    Y_put = fft_price(S0=S0, r=r, q=q, K=K, T=T,
                      model_name='Heston', callput='p', diag=True,
                      sig=sig, th=th, ka=ka, rho=rho, eta=eta)[0]
    try:
        if 0 < Y_put < .6:
            imp_vol = implied_volatility(S0=S0, r=r, q=q, K=K, T=T,
                                         optionType='p', optPrice=Y_put)
            if 0 < imp_vol < .76:
                return '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,P,XXX\n' % (sig, th, ka,
                                                                           rho, eta, S0/K,
                                                                           r, q, T,
                                                                           imp_vol, Y_put, K, S0)
    except Exception as e:
        print(e)
        return ''
    return''
