# -*- coding: utf-8 -*-
"""
Created on Tue May 31 16:09:22 2020.

@author: raul
"""

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense, Layer


def generate_training_set(df_cleaniqr, batch=None, predict=False):
    """Generate the tensor to be fed to the ANN."""
    X_bw = df_cleaniqr[['rate', 'dividend', 'time_maturity']].copy()
    X_bw.loc[:, 'moneyness'] = (df_cleaniqr['price'])/(df_cleaniqr['strike'])
    X_bw.loc[:, 'sig'] = X_bw.loc[:, 'th'] = X_bw.loc[:, 'eta'] = 1
    X_bw.loc[:, 'rho'] = 1
    X_bw.loc[:, 'ka'] = 1
    if predict:
        X_bw[['sig', 'th', 'ka', 'rho', 'eta']
             ] = df_cleaniqr[['sig', 'th', 'ka', 'rho', 'eta']]
        X_bw = X_bw[['sig', 'th', 'ka', 'rho', 'eta', 'moneyness',
                     'rate', 'dividend', 'time_maturity']].values
        return tf.convert_to_tensor(X_bw, dtype='float32')
    Y_bw = df_cleaniqr['iv'].values
    X_bw = X_bw[['sig', 'th', 'ka', 'rho', 'eta', 'moneyness',
                 'rate', 'dividend', 'time_maturity']].values
    X_train_bw, y_train_bw = [tf.convert_to_tensor(
        x_set, dtype='float32') for x_set in [X_bw, Y_bw]]
    train_bw = tf.data.Dataset.from_tensor_slices(
        (X_train_bw, y_train_bw)).shuffle(500)
    if batch:
        return train_bw.batch(batch)
    return train_bw


def has_converged(count, loss, prev_loss, thresshold):
    """Evaluate if the calibration has converged."""
    if prev_loss - loss < thresshold:
        return count+1, loss
    return 0, loss


class InputLayer(Layer):
    """Input layer.

    Receives as input the values for the option plus the parameters for the
    model. The later corresponding neurons can be trained.
    """

    def __init__(self, num_params, num_envals, **kwargs):
        """Define training neurons."""
        super(InputLayer, self).__init__(**kwargs)
        # Create a trainable input layer
        self.paramets = tf.Variable(initial_value=tf.ones((1, num_params),
                                                          dtype='float32'),
                                    trainable=True, name='Heston_parameters',
                                    dtype='float32')
        # Neuron for the Strike, Spot, etc -> we never train this layer
        self.envars = tf.Variable(initial_value=tf.ones((1, num_envals),
                                                        dtype='float32'),
                                  trainable=False, name='envar',
                                  dtype='float32')
        # self.first_lay = tf.concat([self.paramets,self.envars],1)

    def call(self, inputs):
        """Call the layer."""
        first_lay = tf.concat([self.paramets, self.envars], 1)
        return tf.multiply(inputs, first_lay)


class Calnet(tf.keras.Model):
    """End-to-end model for training."""

    def __init__(self,
                 num_params,
                 num_envals,
                 name='calnet',
                 **kwargs):
        super(Calnet, self).__init__(name=name, **kwargs)
        self.num_params = num_params
        self.num_envals = num_envals
        self.in_layer = InputLayer(num_params, num_envals)
        self.d1 = Dense(400, activation=tf.nn.leaky_relu)
        self.d2 = Dense(400, activation=tf.nn.leaky_relu)
        # self.d3 = Dense(400, activation=tf.nn.leaky_relu)
        self.d4 = Dense(400, activation=tf.nn.leaky_relu)
        self.d5 = Dense(400, activation=tf.nn.leaky_relu)
        self.regression = Dense(1, activation='linear')

    def call(self, inputs):
        x = self.in_layer(inputs)
        x = self.d1(x)
        x = self.d2(x)
        #x = self.d3(x)
        x = self.d4(x)
        x = self.d5(x)
        return self.regression(x)


class Ann():
    """Main ANN wrapper."""

    loss_object = tf.keras.losses.MeanSquaredError()
    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_mae = tf.keras.metrics.MeanAbsoluteError(name='train_mae')
    train_mape = tf.keras.metrics.MeanAbsolutePercentageError(
        name='train_mape')
    test_loss = tf.keras.metrics.Mean(name='test_loss')
    test_mae = tf.keras.metrics.MeanAbsoluteError(name='test_mae')
    test_mape = tf.keras.metrics.MeanAbsolutePercentageError(name='test_mape')

    bw_loss = tf.keras.metrics.Mean(name='bw_loss')
    bw_mae = tf.keras.metrics.MeanAbsoluteError(name='bw_mae')
    bw_mape = tf.keras.metrics.MeanAbsolutePercentageError(name='bw_mape')

    bw_test_loss = tf.keras.metrics.Mean(name='test_loss')
    bw_test_mae = tf.keras.metrics.MeanAbsoluteError(name='test_mae')
    bw_test_mape = tf.keras.metrics.MeanAbsolutePercentageError(
        name='test_mape')

    def __init__(self, parameters=5, option_vals=4, train=False,
                 model_path='./tools/slim_model.h5'):
        """Define ANN arquitecture from Calnet."""
        self.model = Calnet(parameters, option_vals)
        self.bw_opimizer = tf.keras.optimizers.Adam()
        # radam = tfa.optimizers.RectifiedAdam(lr=1e-3,
        #                                      total_steps=8000 *
        #                                      len(theoretical_data)//bach_size,
        #                                      warmup_proportion=0.1,
        #                                      min_lr=1e-5)
        #
        # self.optimizer = tfa.optimizers.Lookahead(radam, sync_period=6,
        #                                           slow_step_size=0.5)
        if not train:
            self.model.build((9,))
            self.model.load_weights(model_path)

    @tf.function
    def train_step(self, data, target):
        """Calculate the gradients and update the withs in the forward pass."""
        with tf.GradientTape() as tape:
            predictions = self.model(data, training=True)
            loss = self.loss_object(target, predictions)
        gradients = tape.gradient(loss, self.model.trainable_variables[1:])
        optimizer.apply_gradients(
            zip(gradients, self.model.trainable_variables[1:]))

        self.train_loss(loss)
        self.train_mae(target, predictions)
        self.train_mape(target, predictions)

    @tf.function
    def test_step(self, data, target):
        """Calculate the metrics in the testing step."""
        predictions = self.model(data, training=False)
        t_loss = self.loss_object(target, predictions)

        self.test_loss(t_loss)
        self.test_mae(target, predictions)
        self.test_mape(target, predictions)

    @tf.function
    def test_step_bw(self, data, target):
        """Calculate the testing metrics on the backward pass."""
        predictions = self.model(data, training=False)
        t_loss = self.loss_object(target, predictions)

        self.bw_test_loss(t_loss)
        self.bw_test_mae(target, predictions)
        self.bw_test_mape(target, predictions)

    @tf.function
    def backward_step(self, data, target):
        """Compute the radients and update the weight in the backwards pass."""
        with tf.GradientTape() as tape:
            predictions = self.model(data, training=True)
            loss = self.loss_object(target, predictions)
        gradients = tape.gradient(loss, [self.model.trainable_variables[0]])
        self.bw_opimizer.apply_gradients(
            zip(gradients, [self.model.trainable_variables[0]]))

        self.bw_loss(loss)
        self.bw_mae(target, predictions)
        self.bw_mape(target, predictions)

    def calibrate_heston(self, EPOCHS, train_bw, initial):
        """Calibrate the ANN."""
        temp_weights = self.model.get_weights()
        temp_weights[0][0] = np.array(initial, 'float32')
        self.model.set_weights(temp_weights)
        prev_loss = 1000
        count = 0
        for epoch in range(EPOCHS):
            # Reset the metrics at the start of the next epoch
            self.bw_loss.reset_states()
            self.bw_mae.reset_states()
            self.bw_mape.reset_states()

            for data, target in train_bw:
                self.backward_step(data, target)
            count, prev_loss = has_converged(
                count, self.bw_loss.result(), prev_loss, 5e-10)
            if count > 3:
                break
        return self.bw_loss.result().numpy(), epoch+1

    def calibrate(self, data, initial_params=[.5, .5, .5, -.5, .5]):
        """Calibrate wrap."""
        batch_siz = None
        train_bw = generate_training_set(data, batch=batch_siz)
        mse, nfev = self.calibrate_heston(200, train_bw, initial_params)
        params = list(self.model.get_weights()[0][0])
        return params, mse, nfev

    def predict(self, data):
        """Compute the BS implied volatility arising from Heston price."""
        train_bw = generate_training_set(data, None, True)
        temp_weights = self.model.get_weights()
        initial = [1, 1, 1, 1, 1]
        temp_weights[0][0] = np.array(initial, 'float32')
        self.model.set_weights(temp_weights)
        predictions = self.model(train_bw, training=False)
        if 'iv' in data:
            self.bw_test_loss.reset_states()
            self.bw_test_mae.reset_states()
            self.bw_test_mape.reset_states()
            self.test_step_bw(train_bw, data['iv'].values)
            return predictions, self.bw_test_loss.result().numpy(), self.bw_test_mae.result().numpy(), self.bw_test_mape.result().numpy()
        return predictions.numpy()
