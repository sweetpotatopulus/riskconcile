# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 16:06:39 2020.

This is the code to create theoretical data according to the Heston model.

@author: raul
"""

from time import time
import os.path
from multiprocessing import Pool
from functools import reduce
from pandas import read_csv
from numpy.random import rand as rand
import numpy as np
from tools.utils import implied_volatility, fft_price, get_printable


def get_data_parallet(filename, data_points):
    """Generate new training data randomly."""
    # Declaring the constraints for the variables
    r = (rand(data_points)/100*5).round(4)
    q = (rand(data_points)/100*5).round(4)
    K = [1]*data_points
    S0 = ((rand(data_points)*(1.4-.6))+.6).round(4)
    T = ((rand(data_points)*(3-.05))+.05).round(4)
    v0 = ((rand(data_points)*(.5-.05))+.05)
    sig = (np.sqrt(v0)).round(4)
    th = (rand(data_points)*(.8-.01) + .01).round(4)
    ka = (rand(data_points)*3).round(4)
    rho = (rand(data_points)*-.9).round(4)
    eta = ((rand(data_points)*(.5-.01))+.01).round(4)

    if not os.path.isfile(filename):
        with open(filename, 'w') as fd:
            fd.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, underlying\n' %
                     ('sig', 'th', 'ka', 'rho', 'eta', 'moneyness',
                      'rate', 'dividend', 'time_maturity', 'iv', 'eu_price',
                      'strike', 'price', 'call_put'))

    with Pool() as pool:
        results = pool.starmap(get_printable, zip(*(S0, r, q, K, T, sig, th,
                                                    ka, rho, eta)))
    product = reduce((lambda x, y: x + y), results)
    with open(filename, 'a') as fd:
        fd.write(product)


if __name__ == "__main__":
    # Name of the file to save the data
    FILENAME = 'XXX.csv'
    # Meassute the creation time
    START = time()
    # Call: folder, filename, number of rows
    get_data_parallet('./' + FILENAME, 512)
    print('Time took:', time() - START)
