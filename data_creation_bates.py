
"""
Created on Tue Mar 31 16:06:39 2020.

This function creates the theoretical data for the Heston model. As for
now there is an error in the fft_price function (/tools/utils/fft_price) which
is giving negative prices as outputs.

@author: raul
"""

from time import time
from tools.utils import implied_volatility, fft_price
from pathlib import Path
from pandas import read_csv
from numpy.random import rand as rand
import numpy as np
import os.path
from multiprocessing import Pool
from functools import reduce


def get_printable(S0, r, q, K, T, sig, th, ka, rho, eta, lmb, nu, jet):

    Y_put = fft_price(S0=S0, r=r, q=q, K=K, T=T, model_name='Bates',
                      callput='p', sig=sig, th=th, ka=ka, rho=rho, eta=eta,
                      lmb=lmb, nu=nu, jet=jet, diag=True)[0]

    print()

    try:
        if (Y_put > 0) and (Y_put < .8):
            imp_vol = implied_volatility(S0=S0, r=r, q=q, K=K, T=T,
                                         optionType='p', optPrice=Y_put)
            if (imp_vol > 0) and (imp_vol < .76):
                return '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n' % (sig, th,
                                                                        ka, rho,
                                                                        eta, lmb,
                                                                        nu, jet,
                                                                        S0/K, r,
                                                                        q, T,
                                                                        imp_vol,
                                                                        Y_put)
            return '\nVolatility succs***'+str(Y_put)+'***'+str(S0/K)+' '+str(imp_vol)+'***\n'
    except:
        return ''
    return '\nY_put succed***'+str(Y_put)+'***'+str(S0/K)+'***\n' +\
        str((S0, r, q, K, T))+'\n' + \
        str((sig, th, ka, rho, eta, lmb, nu, jet))+'\n'


def get_data_parallet(filename, data_points):
    """
    This is the code to randomly generate new training data
    In the paper they used around 1000000 data points (one million)
    """

    # Declaring the constraints for the variables
    r = (rand(data_points)/100*5).round(4)
    q = (rand(data_points)/100*5).round(4)
    K = [1]*data_points
    S0 = ((rand(data_points)*(1.4-.6))+.6).round(4)
    T = ((rand(data_points)*(3-.05))+.05).round(4)
    v0 = ((rand(data_points)*(.5-.01))+.01)
    sig = (np.sqrt(v0)).round(4)
    th = (rand(data_points)*(.8-.01) + .01).round(5)
    ka = (rand(data_points)*2+1).round(4)
    rho = (rand(data_points)*-.9).round(4)
    eta = ((rand(data_points)*(.5-.01))+.01).round(4)
    lmb_jumps = (rand(data_points)*3).round(4)
    miu = (rand(data_points)*.4).round(5)
    var_jumps = (np.sqrt(rand(data_points)*.3)).round(5)

    if not os.path.isfile(filename):
        with open(filename, 'w') as data_file:
            data_file.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n' %
                            ('sig', 'th', 'ka', 'rho', 'eta', 'lambda', 'miu',
                             'var', 'moneyness', 'r', 'q', 'T', 'implied_vol',
                             'put_price'))

    with Pool() as pool:
        results = pool.starmap(get_printable, zip(*(S0, r, q, K, T, sig, th,
                                                    ka, rho, eta, lmb_jumps,
                                                    miu, var_jumps)))
    product = reduce((lambda x, y: x + y), results)
    with open(filename, 'a') as fd:
        fd.write(product)


if __name__ == "__main__":
    FILENAME = 'bates/test.csv'
    Path('bates').mkdir(parents=True, exist_ok=True)
    START = time()
    get_data_parallet('./'+FILENAME, 5)
    print('Time took:', time()-START)
